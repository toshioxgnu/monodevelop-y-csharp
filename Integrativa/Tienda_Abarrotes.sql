CREATE SCHEMA `TIENDA_ABARROTES` ;

create table tbProductos;

insert
into
	tbProductos
(
	ID
,	Nombre
,	Precio
,	Cantidad
) values (
	'10'
,	'Pack de Cervezas Baltica'
,	'2990'
,	'45'
);insert
into
	tbProductos
(
	ID
,	Nombre
,	Precio
,	Cantidad
) values (
	'777757813485'
,	'Hallulla Corriente'
,	'1290'
,	'500'
);insert
into
	tbProductos
(
	ID
,	Nombre
,	Precio
,	Cantidad
) values (
	'5489615'
,	'Bebida Gatorade'
,	'990'
,	'123'
);insert
into
	tbProductos
(
	ID
,	Nombre
,	Precio
,	Cantidad
) values (
	'20'
,	'Pañales Babysec'
,	'9990'
,	'100'
);insert
into
	tbProductos
(
	ID
,	Nombre
,	Precio
,	Cantidad
) values (
	'5654443'
,	'Desodorante Axe'
,	'1990'
,	'45'
);insert
into
	tbProductos
(
	ID
,	Nombre
,	Precio
,	Cantidad
) values (
	'30'
,	'Vino Alto Del Carmen'
,	'3990'
,	'40'
);insert
into
	tbProductos
(
	ID
,	Nombre
,	Precio
,	Cantidad
) values (
	'77777123453'
,	'Marraqueta Corriente'
,	'1290'
,	'600'
);insert
into
	tbProductos
(
	ID
,	Nombre
,	Precio
,	Cantidad
) values (
	'154567'
,	'Arroz PreGraneado Tucapel 1K'
,	'790'
,	'500'
);insert
into
	tbProductos
(
	ID
,	Nombre
,	Precio
,	Cantidad
) values (
	'7777745685132'
,	'Queso Gauda'
,	'3990'
,	'100'
);insert
into
	tbProductos
(
	ID
,	Nombre
,	Precio
,	Cantidad
) values (
	'7777745821'
,	'Pack de Cervezas Cristal'
,	'2990'
,	'45'
);insert
into
	tbProductos
(
	ID
,	Nombre
,	Precio
,	Cantidad
) values (
	'77777451213'
,	'Toallitas Higienicas'
,	'1990'
,	'50'
);insert
into
	tbProductos
(
	ID
,	Nombre
,	Precio
,	Cantidad
) values (
	'412345'
,	'Super 8'
,	'300'
,	'250'
);insert
into
	tbProductos
(
	ID
,	Nombre
,	Precio
,	Cantidad
) values (
	'4521134'
,	'Zapatillas Deportivas'
,	'15990'
,	'100'
);insert
into
	tbProductos
(
	ID
,	Nombre
,	Precio
,	Cantidad
) values (
	'36542'
,	'Agua Mineral'
,	'590'
,	'100'
);insert
into
	tbProductos
(
	ID
,	Nombre
,	Precio
,	Cantidad
) values (
	'451565'
,	'Whiskas 3 kilos'
,	'3890'
,	'450'
);insert
into
	tbProductos
(
	ID
,	Nombre
,	Precio
,	Cantidad
) values (
	'4512334'
,	'Ramen Xtra Hot XiaoPeng'
,	'1590'
,	'200'
);insert
into
	tbProductos
(
	ID
,	Nombre
,	Precio
,	Cantidad
) values (
	'7777715312'
,	'Lapiz BIC azul'
,	'190'
,	'500'
);insert
into
	tbProductos
(
	ID
,	Nombre
,	Precio
,	Cantidad
) values (
	'12451321'
,	'Limon a Granel'
,	'600'
,	'700'
);insert
into
	tbProductos
(
	ID
,	Nombre
,	Precio
,	Cantidad
) values (
	'777775638746'
,	'Pisco Mistral 750cc'
,	'4990'
,	'45'
);insert
into
	tbProductos
(
	ID
,	Nombre
,	Precio
,	Cantidad
) values (
	'4513213'
,	'Pack de Cervezas BudWeiser'
,	'5990'
,	'500'
);insert
into
	tbProductos
(
	ID
,	Nombre
,	Precio
,	Cantidad
) values (
	'15452135'
,	'Champion Katt'
,	'18990'
,	'48'
);
