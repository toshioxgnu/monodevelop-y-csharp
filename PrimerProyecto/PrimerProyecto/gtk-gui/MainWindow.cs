// This file has been generated by the GUI designer. Do not modify.

public partial class MainWindow {
    private global::Gtk.Fixed fixed1;

    private global::Gtk.CheckButton checkbutton1;

    private global::Gtk.Button button1;

    private global::Gtk.CheckButton checkbutton2;

    private global::Gtk.CheckButton checkbutton3;

    protected virtual void Build () {
        global::Stetic.Gui.Initialize (this);
        // Widget MainWindow
        this.Name = "MainWindow";
        this.Title = global::Mono.Unix.Catalog.GetString ("MainWindow");
        this.WindowPosition = ((global::Gtk.WindowPosition) (4));
        this.AllowShrink = true;
        // Container child MainWindow.Gtk.Container+ContainerChild
        this.fixed1 = new global::Gtk.Fixed ();
        this.fixed1.Name = "fixed1";
        this.fixed1.HasWindow = false;
        // Container child fixed1.Gtk.Fixed+FixedChild
        this.checkbutton1 = new global::Gtk.CheckButton ();
        this.checkbutton1.CanFocus = true;
        this.checkbutton1.Name = "checkbutton1";
        this.checkbutton1.Label = global::Mono.Unix.Catalog.GetString ("checkbutton1");
        this.checkbutton1.DrawIndicator = true;
        this.checkbutton1.UseUnderline = true;
        this.fixed1.Add (this.checkbutton1);
        global::Gtk.Fixed.FixedChild w1 = ((global::Gtk.Fixed.FixedChild) (this.fixed1[this.checkbutton1]));
        w1.X = 9;
        w1.Y = 14;
        // Container child fixed1.Gtk.Fixed+FixedChild
        this.button1 = new global::Gtk.Button ();
        this.button1.CanFocus = true;
        this.button1.Name = "button1";
        this.button1.UseUnderline = true;
        this.button1.Label = global::Mono.Unix.Catalog.GetString ("GtkButton");
        this.fixed1.Add (this.button1);
        global::Gtk.Fixed.FixedChild w2 = ((global::Gtk.Fixed.FixedChild) (this.fixed1[this.button1]));
        w2.X = 21;
        w2.Y = 182;
        // Container child fixed1.Gtk.Fixed+FixedChild
        this.checkbutton2 = new global::Gtk.CheckButton ();
        this.checkbutton2.CanFocus = true;
        this.checkbutton2.Name = "checkbutton2";
        this.checkbutton2.Label = global::Mono.Unix.Catalog.GetString ("checkbutton1");
        this.checkbutton2.DrawIndicator = true;
        this.checkbutton2.UseUnderline = true;
        this.fixed1.Add (this.checkbutton2);
        global::Gtk.Fixed.FixedChild w3 = ((global::Gtk.Fixed.FixedChild) (this.fixed1[this.checkbutton2]));
        w3.X = 10;
        w3.Y = 47;
        // Container child fixed1.Gtk.Fixed+FixedChild
        this.checkbutton3 = new global::Gtk.CheckButton ();
        this.checkbutton3.CanFocus = true;
        this.checkbutton3.Name = "checkbutton3";
        this.checkbutton3.Label = global::Mono.Unix.Catalog.GetString ("checkbutton2");
        this.checkbutton3.DrawIndicator = true;
        this.checkbutton3.UseUnderline = true;
        this.fixed1.Add (this.checkbutton3);
        global::Gtk.Fixed.FixedChild w4 = ((global::Gtk.Fixed.FixedChild) (this.fixed1[this.checkbutton3]));
        w4.X = 9;
        w4.Y = 79;
        this.Add (this.fixed1);
        if ((this.Child != null)) {
            this.Child.ShowAll ();
        }
        this.DefaultWidth = 189;
        this.DefaultHeight = 300;
        this.Show ();
        this.DeleteEvent += new global::Gtk.DeleteEventHandler (this.OnDeleteEvent);
        this.button1.Clicked += new global::System.EventHandler (this.OnButton1Clicked);
    }
}